export default class VoiceSynthesis {
  constructor(textToSpeak) {
    this.synthesisApiObject = new SpeechSynthesisUtterance();
    this.synthesisApiObject.volume = 1;
    this.synthesisApiObject.rate = 1;
    this.synthesisApiObject.lang = 'pl-PL';
    this.synthesisApiObject.text = textToSpeak;
  }

  setMessage(newMessage) {
    this.synthesisApiObject.text = newMessage;
  }

  sayMessage() {
    if (window.speechSynthesis) {
      window.speechSynthesis.speak(this.synthesisApiObject);
    } else {
      throw('There isn\'t any speech synthesis method');
    }
  }
}