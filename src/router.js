import Vue from 'vue'
import Router from 'vue-router'
import Index from './views/Index.vue'
import Knowlagde from './views/Quiz/Knowlagde.vue'
import Subcategory from './views/Quiz/Subcategory.vue'
import Slides from './views/Quiz/Slides.vue'
import Game from './views/Quiz/Game.vue'
import MainFactory from './PageFactory/MainFactory.vue';
import QR from './views/QR.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  },
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'index',
      component: Index,
      meta: {
        headerText: 'Pomoc'
      }
    },
    {
      path: '/pomoc',
      name: 'help',
      component: () => import('./views/Help.vue'),
      meta: {
        headerText: 'Pomoc'
      }
    },
    {
      path: '/wiedza',
      name: 'quiz',
      component: Knowlagde,
      meta: {
        headerText: 'Wiedza'
      }
    },
    {
      path: '/places',
      name: 'places',
      component: () => import('./views/Places.vue')
    },
    {
      path: '/cpr',
      name: 'cpr',
      component: () => import('./views/CPR/cpr.vue')
    },
    {
      path: '/wiedza/:slug',
      name: 'quiz-category',
      component: Subcategory,
      props: true,
      meta: {
        headerText: 'Wiedza'
      }
    },
    {
      path: '/prezentacje/:id',
      name: 'quiz-slides',
      component: Slides,
      props: true,
      meta: {
        headerText: 'Wiedza'
      }
    },
    {
      path: '/quiz/:id',
      name: 'quiz-game',
      component: Game,
      props: true,
      meta: {
        headerText: 'Wiedza'
      }
    },
    {
      path: '/co-robic/:id',
      name: 'co-robic',
      component: MainFactory,
      props: true,
      meta: {
        headerText: 'Pomoc'
      }
    },
    {
      path: '/kod-qr',
      name: 'qr',
      component: QR,
      props: true,
      meta: {
        headerText: 'Kod QR'
      }
    }
  ]
})
